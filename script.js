let submitBtn = document.querySelector(".submitBtn");
let form = document.getElementById("form");

form.addEventListener('submit', (e) => {
    e.preventDefault();

    let validatePassword = false;
    let validateRole = false;
    let roleSelected;
    let genderVal = e.target[2].value;

    let passRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$/;
    let passValue = e.target[1].value;

    if (passValue.match(passRegex)) {
        validatePassword = true;
    } else {
        alert("password should be of minimum 6 charactes with a mix of uppercase lowercase and digit in it");
    }


    if (e.target[3].checked || e.target[4].checked) {
        validateRole = true;
        if (e.target[3].checked) {
            roleSelected = e.target[3].value;
        } else {
            roleSelected = e.target[4].value;
        }
    } else {
        alert("please select your role");
    }

    let checkBoxCount = 0;
    let checkBoxValueArr = [];
    let validatePermission = false;
    for (let i = 5; i < form.elements.length - 1; i++) {
        if (e.target[i].checked) {
            checkBoxCount++;
            checkBoxValueArr.push(e.target[i].value);
        }
    }

    if (checkBoxCount < 2) {
        alert("Atleast 2 permissions should be ticked");
    } else {
        validatePermission = true;
    }

    if (validatePassword && validatePermission && validateRole) {
        form.innerHTML = `
                            <h3>Email: ${e.target[0].value}</h3>
                            <h3>Password: ${passValue}</h3>
                            <h3>Sex: ${genderVal}</h3>
                            <h3>Role: ${roleSelected}</h3>
                            <h3>Permissions: ${checkBoxValueArr}</h3>
                            <button>Confirm</button>
                            `
    }
})
